package com.rave.quizapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class QuizApplication : Application()
//{
//
//    override fun onCreate() {
//        super.onCreate()
//
//        // Pre-load the database with initial data
//        val request = OneTimeWorkRequestBuilder<PreloadDatabaseWorker>().build()
//        WorkManager.getInstance(this).enqueue(request)
//    }
//}
