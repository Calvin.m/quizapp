package com.rave.quizapp.viewmodel

import android.content.ContentValues.TAG
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.quizapp.model.Quiz
import com.rave.quizapp.model.QuizRepo
import com.rave.quizapp.view.QuizListState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class QuizListViewModel @Inject constructor(val repo: QuizRepo) : ViewModel() {

    private val _state = MutableLiveData(QuizListState())
    val state: LiveData<QuizListState> get() = _state

    init {
        viewModelScope.launch {
            repo.preloadQuizzes()
            repo.preloadQuestions()
        }
        fetchQuizzes()
    }

    fun fetchQuizzes() {
        viewModelScope.launch {
            _state.value = _state.value?.copy(isLoading = true)
            val list = repo.getQuizzes()
            _state.value = _state.value?.copy(isLoading = false, quizList = list)
        }
    }
}