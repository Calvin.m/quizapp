package com.rave.quizapp.viewmodel

import android.content.ContentValues.TAG
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.quizapp.model.QuizRepo
import com.rave.quizapp.model.QuizResult
import com.rave.quizapp.view.QuizResultState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.collections.average


@HiltViewModel
class QuizResultViewModel @Inject constructor(private val repo: QuizRepo) : ViewModel() {

    private val _state = MutableLiveData(QuizResultState())
    val state: LiveData<QuizResultState> get() = _state

    fun getResults() {
        viewModelScope.launch {
            _state.value = _state.value?.copy(isLoading = true)
            val list = repo.getResults()
            _state.value = _state.value?.copy(isLoading = false, resultsList = list)
        }
    }

    fun calcAndGetResult(userAnswers: String) {
        viewModelScope.launch {
            _state.value = _state.value?.copy(isLoading = true)

            val answers = repo.getQuestions(0)

            val correctAnswers = answers.map {
                it.correctAnswer
            }

            val rightAnswers = correctAnswers.map {
                it[0].split("[")[1].trim()
                    .split("]")[0].trim()
                    .split(",")
            }

            val givenAnswers = userAnswers.split("[")[1].trim()
                .split("]")[0].trim()
                .split(",")

            val keywordCount = rightAnswers.mapIndexed { index, strings ->
                val cnt = strings.count { keyword ->
                    givenAnswers[index].contains(keyword, ignoreCase = true)
                    // todo - manually insert list of "bad words" for each file
                    // todo - use "contains" again but mark them as "-1"
                }
                cnt / rightAnswers[index].size.toDouble()
            }

            val newResult = QuizResult(
                null,
                quizId = 0,
                correctAnswer = correctAnswers[0],
                userAnswer = userAnswers,
                grade = (keywordCount.average() * 100).toInt()
            )
            repo.insertResult(newResult)
            _state.value = _state.value?.copy(isLoading = false)
        }
    }
}
