package com.rave.quizapp.viewmodel

import android.content.ContentValues.TAG
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.quizapp.model.Question
import com.rave.quizapp.model.QuizRepo
import com.rave.quizapp.view.QuizState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class QuizViewModel @Inject constructor(val repo: QuizRepo) : ViewModel() {

    private val _state = MutableLiveData(QuizState())
    val state: LiveData<QuizState> get() = _state

    fun fetchQuestions(quizId: Long) {
        viewModelScope.launch {
            _state.value = _state.value?.copy(isLoading = true)
            val list = repo.getQuestions(quizId)
            _state.value = _state.value?.copy(isLoading = false, questionList = list)
        }
    }

    fun updateQuestion(question: Question) {
        viewModelScope.launch {
            _state.value = _state.value?.copy(isLoading = true)
            repo.updateQuestion(question)
            _state.value = _state.value?.copy(isLoading = false)
        }
    }
}