package com.rave.quizapp.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters

@Entity(tableName = "result_table")
data class QuizResult(
//    @PrimaryKey val resultId: Long,
    @PrimaryKey(autoGenerate = true) val resultId: Long? = 0,
    val quizId: Long,
    @TypeConverters(CorrectAnswerConverter::class)
    val correctAnswer: List<String>,
//    val correctAnswer: List<String>,
    val userAnswer: String? = "",
    val grade: Int
)