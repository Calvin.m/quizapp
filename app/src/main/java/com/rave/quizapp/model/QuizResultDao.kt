package com.rave.quizapp.model

import androidx.room.*

@Dao
interface QuizResultDao {
    @Insert
    suspend fun insert(quizResult: QuizResult)
    @Query("SELECT * FROM result_table")
    suspend fun getResults(): List<QuizResult>
//
//    @Insert
//    suspend fun insert(result: QuizResult)
//
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    suspend fun insertAll(results: List<QuizResult>)
//
//    @Update
//    suspend fun update(result: QuizResult)
//
//    @Delete
//    suspend fun delete(result: QuizResult)
}