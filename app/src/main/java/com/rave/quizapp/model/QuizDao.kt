package com.rave.quizapp.model

import androidx.room.*

@Dao
interface QuizDao {
    @Query("SELECT * FROM quiz")
    suspend fun getAllQuizzes(): List<Quiz>

    @Query("SELECT * FROM quiz WHERE quizId = :quizId")
    suspend fun getQuiz(quizId: Long): Quiz

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(quiz: Quiz)

    @Update
    suspend fun update(quiz: Quiz)

    @Delete
    suspend fun delete(quiz: Quiz)
}
