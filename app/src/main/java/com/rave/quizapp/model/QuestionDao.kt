package com.rave.quizapp.model

import androidx.room.*

@Dao
interface QuestionDao {
    @Query("SELECT * FROM question WHERE quizId = :quizId")
    suspend fun getQuestions(quizId: Long): List<Question>

    @Query("SELECT * FROM question WHERE questionId = :questionId")
    suspend fun getQuestion(questionId: Long): Question

    @Insert
    suspend fun insert(question: Question)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(questions: List<Question>)

    @Update
    suspend fun update(question: Question)

    @Delete
    suspend fun delete(question: Question)
}
