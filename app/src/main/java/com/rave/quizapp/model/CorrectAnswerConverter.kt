package com.rave.quizapp.model

import android.content.ContentValues.TAG
import android.util.Log
import androidx.room.TypeConverter

class CorrectAnswerConverter {
    @TypeConverter
    fun fromCorrectAnswer(correctAnswer: List<String>): String {
        return correctAnswer.toString()
    }

    @TypeConverter
    fun toCorrectAnswer(data: String): List<String> {
        return listOf(data)
    }
}