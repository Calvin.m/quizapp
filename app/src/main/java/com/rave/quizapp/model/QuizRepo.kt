package com.rave.quizapp.model

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class QuizRepo @Inject constructor(db: AppDatabase) {

    private val quizDao = db.quizDao()
    private val questionDao = db.questionDao()
    private val quizResultDao = db.quizResultDao()


    suspend fun getQuizzes(): List<Quiz> = withContext(Dispatchers.IO) {
        quizDao.getAllQuizzes()
    }

    suspend fun getQuestions(quizId: Long): List<Question> = withContext(Dispatchers.IO) {
        questionDao.getQuestions(quizId)
    }

    suspend fun updateQuestion(question: Question) = withContext(Dispatchers.IO) {
        questionDao.update(question)
    }

    suspend fun insertResult(quizResult: QuizResult) = withContext(Dispatchers.IO) {
        quizResultDao.insert(quizResult)
    }

    suspend fun getResults() = withContext(Dispatchers.IO) {
        quizResultDao.getResults()
    }


    suspend fun preloadQuizzes() {
        quizDao.insert(
            Quiz(
                title = "Android Quiz",
                description = "Quiz to help with interviews for android developer posisitons",
                quizId = 0,
                category = "android"
            )
        )
    }

    suspend fun preloadQuestions() {
        questionDao.insertAll(
            listOf(
                Question(0, 0, "1. Which scopes use 'it'?", listOf("also", "let")),
                Question(1, 0, "2. Which scopes use 'this'?", listOf("apply", "with", "run"))
            )
        )
    }
}
