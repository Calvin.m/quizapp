//package com.rave.quizapp.model
//
//import android.content.Context
//import androidx.work.Worker
//import androidx.work.WorkerParameters
//
//class PreloadDatabaseWorker(
//    context: Context,
//    params: WorkerParameters
//) : Worker(context, params) {
//
//    override fun doWork(): Result {
//        val quizDao = AppDatabase.getInstance(applicationContext).quizDao()
//        val questionDao = AppDatabase.getInstance(applicationContext).questionDao()
//
//        // Insert initial quizzes
//        val quizzes = listOf(
//            Quiz(
//                title = "Android Quiz",
//                description = "Quiz to help with interviews for android developer posisitons",
//                id = 0,
//                category = "android"
//            )
////            Quiz(title = "Quiz 2", description = "Description 2"),
////            Quiz(title = "Quiz 3", description = "Description 3")
//        )
////        quizDao.insertAll(quizzes)
//        quizDao.insert(
//            Quiz(
//                title = "Android Quiz",
//                description = "Quiz to help with interviews for android developer posisitons",
//                id = 0,
//                category = "android"
//            )
//        )
//
//        // Insert initial questions
//        val questions = listOf(
//            Question(quizId = quizzes[0].id, text = "Question 1", correctAnswer = "Answer 1"),
//            Question(quizId = quizzes[0].id, text = "Question 2", correctAnswer = "Answer 2"),
//            Question(quizId = quizzes[0].id, text = "Question 3", correctAnswer = "Answer 3"),
//            Question(quizId = quizzes[1].id, text = "Question 4", correctAnswer = "Answer 4"),
//            Question(quizId = quizzes[1].id, text = "Question 5", correctAnswer = "Answer 5"),
//            Question(quizId = quizzes[2].id, text = "Question 6", correctAnswer = "Answer 6")
//        )
//        questionDao.insertAll(questions)
//
//        return Result.success()
//    }
//}
