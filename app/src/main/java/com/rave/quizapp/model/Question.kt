package com.rave.quizapp.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters

@Entity
data class Question(
    @PrimaryKey val questionId: Long,
    val quizId: Long,
    val text: String,
    @TypeConverters(CorrectAnswerConverter::class)
    val correctAnswer: List<String>,
    val userAnswer: String? = ""
)
