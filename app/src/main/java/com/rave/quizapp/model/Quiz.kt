package com.rave.quizapp.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Quiz(
    @PrimaryKey val quizId: Long,
    val title: String,
    val category: String,
    val description: String
)
