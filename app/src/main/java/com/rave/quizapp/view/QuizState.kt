package com.rave.quizapp.view

import com.rave.quizapp.model.Question

data class QuizState(
    val isLoading: Boolean = false,
    val questionList: List<Question> = emptyList(),
    val correctAnswerList: List<String> = emptyList()
)