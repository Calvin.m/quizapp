package com.rave.quizapp.view

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.rave.quizapp.databinding.FragmentResultBinding
import com.rave.quizapp.viewmodel.QuizResultViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ResultFragment : Fragment() {

    private val resultViewModel by viewModels<QuizResultViewModel>()
    private var _binding: FragmentResultBinding? = null
    private val resultAdapter by lazy { ResultAdapter() }
    private val answersList by lazy { arguments?.getString("answersList") }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        resultViewModel.getResults()
        val usersAnswers = answersList
        resultViewModel.calcAndGetResult(usersAnswers ?: "")
        return FragmentResultBinding.inflate(inflater, container, false).apply {
            _binding = this
            rvResults.adapter = this@ResultFragment.resultAdapter
            rvResults.layoutManager = LinearLayoutManager(context)
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        resultViewModel.state.observe(viewLifecycleOwner) { state ->
            resultAdapter.loadResults(state.resultsList)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
