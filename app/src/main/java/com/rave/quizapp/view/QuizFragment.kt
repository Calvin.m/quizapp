package com.rave.quizapp.view

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rave.quizapp.R
import com.rave.quizapp.databinding.FragmentQuizBinding
import com.rave.quizapp.view.QuizAdapter
import com.rave.quizapp.viewmodel.QuizViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class QuizFragment : Fragment() {
    //    @ViewModelInject
    private val quizViewModel by viewModels<QuizViewModel>()
    private var _binding: FragmentQuizBinding? = null
    private val answersList: MutableList<String> = mutableListOf("", "")
    private val quizAdapter by lazy {
        QuizAdapter { question ->
            answersList[question.questionId.toInt()] = question.userAnswer ?: ""
        }
    }
    private val quizId by lazy { arguments?.getString("quizId") }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val qList = quizViewModel.fetchQuestions(quizId?.toLong() ?: 0)

        quizViewModel.state.observe(viewLifecycleOwner) { state ->
            quizAdapter.loadQuestions(state.questionList)
        }

        return FragmentQuizBinding.inflate(inflater, container, false).apply {
            _binding = this
            rvQuestions.adapter = this@QuizFragment.quizAdapter
            rvQuestions.layoutManager = LinearLayoutManager(context)
            submitButton.setOnClickListener {
                val state = quizViewModel.state.observe(viewLifecycleOwner) { state ->
                    val args = bundleOf("answersList" to answersList.toString())
                    findNavController().navigate(
                        resId = R.id.action_quizFragment_to_resultFragment,
                        args
                    )
                }
            }
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // ...
    }
}