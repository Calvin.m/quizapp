package com.rave.quizapp.view

import android.content.ContentValues.TAG
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.RecyclerView
import com.rave.quizapp.databinding.QuizQuestionItemBinding
import com.rave.quizapp.model.Question
import com.rave.quizapp.viewmodel.QuizViewModel

class QuizAdapter(
    private val questionUpdated: (Question) -> Unit
) : RecyclerView.Adapter<QuizAdapter.QuizViewHolder>() {

    private val quizQuestions: MutableList<Question> = mutableListOf()

    class QuizViewHolder(val binding: QuizQuestionItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val question = binding.quizQuestion
        val userAnswer = binding.userAnswer
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuizViewHolder {
        return QuizViewHolder(
            QuizQuestionItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: QuizViewHolder, position: Int) {
        val currentQuestion = quizQuestions[position]
        holder.question.text = currentQuestion.text
        holder.userAnswer.addTextChangedListener { str ->
            questionUpdated(
                Question(
                    currentQuestion.questionId,
                    currentQuestion.quizId,
                    currentQuestion.text,
                    currentQuestion.correctAnswer,
                    str.toString()
                )
            )
        }
    }

    override fun getItemCount(): Int = quizQuestions.size

    fun loadQuestions(newQs: List<Question>) {
        val oldSize = quizQuestions.size
        quizQuestions.clear()
        notifyItemRangeRemoved(0, oldSize)
        quizQuestions.addAll(newQs)
        notifyItemRangeInserted(0, newQs.size)
    }
}