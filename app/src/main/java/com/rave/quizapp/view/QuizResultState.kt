package com.rave.quizapp.view

import com.rave.quizapp.model.QuizResult

data class QuizResultState(
    val isLoading: Boolean = false,
    val correctAnswers: List<String> = emptyList(),
    val userAnswers: List<String> = emptyList(),
    val grade: Int = 0,
    val quizResultList: List<QuizResult> = emptyList(),
    val resultsList: List<QuizResult> = emptyList()
)
