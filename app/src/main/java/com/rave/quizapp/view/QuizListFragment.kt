package com.rave.quizapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rave.quizapp.R
import com.rave.quizapp.databinding.FragmentQuizListBinding
import com.rave.quizapp.model.Quiz
import com.rave.quizapp.viewmodel.QuizListViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class QuizListFragment : Fragment() {

    private val quizListViewModel by viewModels<QuizListViewModel>()
    private var _binding: FragmentQuizListBinding? = null
    private val quizListAdapter by lazy { QuizListAdapter {quiz: Quiz ->
        val args = bundleOf("quizId" to quiz.quizId)
        findNavController().navigate(
            resId = R.id.action_quizListFragment_to_quizFragment,
            args
        )
    } }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return FragmentQuizListBinding.inflate(inflater, container, false).apply {
            _binding = this
            rvQuizzes.adapter = this@QuizListFragment.quizListAdapter
            rvQuizzes.layoutManager = LinearLayoutManager(context)
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        quizListViewModel.state.observe(viewLifecycleOwner) { state ->
            quizListAdapter.loadQuizzes(state.quizList)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
