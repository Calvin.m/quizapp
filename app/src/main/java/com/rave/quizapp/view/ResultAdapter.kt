package com.rave.quizapp.view

import android.content.ContentValues.TAG
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rave.quizapp.databinding.ResultItemBinding
import com.rave.quizapp.model.QuizResult

class ResultAdapter : RecyclerView.Adapter<ResultAdapter.ResultViewHolder>() {

    private val resultList: MutableList<QuizResult> = mutableListOf()

    class ResultViewHolder(val binding: ResultItemBinding) : RecyclerView.ViewHolder(binding.root) {
        val grade = binding.quizGrade
        val resultNumber = binding.resultNumber
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResultViewHolder {
        return ResultViewHolder(
            ResultItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ResultViewHolder, position: Int) {
        holder.grade.text = "${resultList[position].grade.toString()}%"
        holder.resultNumber.text = "Try #${resultList.size - position}"
    }

    override fun getItemCount(): Int = resultList.size

    fun loadResults(newResults: List<QuizResult>) {
        val oldSize = resultList.size
        resultList.clear()
        notifyItemRangeRemoved(0, oldSize)
        resultList.addAll(newResults.reversed())
        notifyItemRangeInserted(0, newResults.size)
    }
}