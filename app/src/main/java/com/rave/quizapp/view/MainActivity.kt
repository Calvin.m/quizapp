package com.rave.quizapp.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.rave.quizapp.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(R.layout.activity_main)
//{
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
//
//        if (savedInstanceState == null) {
//            supportFragmentManager.beginTransaction()
//                .replace(R.id.container, QuizListFragment.newInstance())
//                .commitNow()
//        }
//    }
//}
