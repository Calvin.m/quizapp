package com.rave.quizapp.view

import com.rave.quizapp.model.Question
import com.rave.quizapp.model.Quiz

data class QuizListState(
    val isLoading: Boolean = false,
    val quizList: List<Quiz> = emptyList()
)