package com.rave.quizapp.view

import android.content.ContentValues.TAG
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rave.quizapp.databinding.FragmentQuizListBinding
import com.rave.quizapp.databinding.QuizListItemBinding
import com.rave.quizapp.model.Quiz

class QuizListAdapter(
    private val quizClicked: (Quiz) -> Unit
) : RecyclerView.Adapter<QuizListAdapter.QuizListViewHolder>() {

    private val quizList: MutableList<Quiz> = mutableListOf()

    class QuizListViewHolder(val binding: QuizListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val quizTitle = binding.quizTitle
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuizListViewHolder {
        return QuizListViewHolder(
            QuizListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        ).apply {
            binding.quizTitle.setOnClickListener {
                val quizSelected = quizList[adapterPosition]
                quizClicked(quizSelected)
            }
        }
    }

    override fun onBindViewHolder(holder: QuizListViewHolder, position: Int) {
        holder.quizTitle.text = quizList[position].title
    }

    override fun getItemCount(): Int = quizList.size

    fun loadQuizzes(newQuizzes: List<Quiz>) {
        quizList.clear()
        quizList.addAll(newQuizzes)
        notifyItemRangeInserted(0, newQuizzes.size)
    }
}